" Indentation/Tab
set autoindent
set tabstop=4
set shiftwidth=4
set noexpandtab
filetype plugin on

" Interface
set number
set ruler
set showcmd
set wildmenu
set colorcolumn=80

" Colors
set nohlsearch
set background=dark
syntax enable

" Usability
set hidden
set lazyredraw
set ttyfast
